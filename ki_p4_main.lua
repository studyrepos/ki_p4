-- KI P4 Main - Progressive Planning

dofile("pddl.lua")
dofile("Class.lua")
dofile("node.lua")
dofile("ProPlanner.lua")

local domain_file = assert(io.open("blocksworld.pddl", "r"))
local problem_file = assert(io.open("./pb3.pddl", "r"))
local ddecl = domain_file:read("*all")
local pdecl = problem_file:read("*all")

local d = get_domain(ddecl)
local p = get_problem(pdecl)

--pretty_print_domain(d)
--pretty_print_problem(p)

local planner = ProPlanner:new(d, p)

local gs = "Goal: ( "
for _,u in pairs(planner.goal) do
    gs = gs .. u .. " " 
end
print(gs .. ")")

print("Run BFS...")
local node = planner:run_bfs()

local s = "State: ("
for _,v in pairs(node.data.state) do
  s = s .. v .. " " 
end
print(s .. ") -> " .. tostring(node.data.goal))

local plan = node.data.action.name .. "(" .. node.data.args[1]
if node.data.args[2] ~= nil then plan = plan .. ", " .. node.data.args[2] end
plan = plan .. ")"
while node.parent ~= nil do
  local act = "start()"
  if node.parent.data.action ~= nil then
    act = node.parent.data.action.name .. "(" .. node.parent.data.args[1]
    if node.parent.data.args[2] ~= nil then act = act .. ", " .. node.parent.data.args[2] end
    act = act .. ")"
  end
  plan = act .. " -> " .. plan
  node = node.parent
end
print("Plan: " .. plan)

