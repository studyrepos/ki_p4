-- Node
dofile("Class.lua")

Node = {}
local Node_mt = Class(Node)


-- Node
function Node:new(data, weight, parent)
  local data = data or nil
  local weight = weight or nil
  local parent = parent or nil
  
  return setmetatable( {
      data = data, 
      weight = weight, 
      parent = parent,}, Node_mt)
end

function Node:equals(arg, value)
  if arg == nil then return false end
  if value then
    return self.data == arg.data and self.weight == arg.weight
  else
    return self.data == arg.data
  end
end

-- comparison result
-- self <= node : true
-- self > node  : false
function Node:compare(node)
  if node == nil then
    return true
  else
    return self.weight <= node.weight
  end
end

function Node:toString()
  return self.data
end

function Node:print()
  print('Node:', self:toString())
end


