-- queue

dofile("node.lua")

function quickSort(array, p, r)
    p = p or 1
    r = r or #array
    if p < r then
        q = partition(array, p, r)
        quickSort(array, p, q - 1)
        quickSort(array, q + 1, r)
    end
end

function partition(array, p, r)
    local x = array[r]
    local i = p - 1
    for j = p, r - 1 do
        if array[j]:compare(x) then
            i = i + 1
            local temp = array[i]
            array[i] = array[j]
            array[j] = temp
        end
    end
    local temp = array[i + 1]
    array[i + 1] = array[r]
    array[r] = temp
    return i + 1
end



Queue = {}
local Queue_mt = Class(Queue)


-- Queue
-- modes: 0 - FIFO
--        1 - LIFO
--        2 - PRIO
function Queue:new(mode)
  local mode = mode or 0
  local data = {}
  local first = 1
  local last  = 0
  
  return setmetatable( {mode = mode, data = data, first = first, last = last}, Queue_mt)
end

function Queue:push(item)
  self.last = self.last + 1
  self.data[self.last] = item
  
  if self.mode == 2 then
    if self.last - self.first > 0 then
      self:sort(self.data)
    end
  end
end

function Queue:getHighest(item)
  local highest = nil
  if self:contains(item) then
    for i=self.first, self.last do
      if self.data[i]:equals(item) then
        if highest == nil 
          or highest:compare(self.data[i]) then
          highest = self.data[i]
        end
      end
    end
  end
  return highest
end


function Queue:pop()
  -- queue is empty
  if self.last < self.first then
    return nil
  end
  -- switch modes
  local item = nil
  -- FIFO or PRIO
  if self.mode == 0 or self.mode == 2 then
    item = self.data[self.first]
--    table.remove(self.data, self.first)
    self.first = self.first + 1
  -- LIFO
  else
    item = self.data[self.last]
    table.remove(self.data, self.last)
    self.last = self.last - 1
  end
  return item
end

function Queue:replace(item, newItem)
  for i=self.first, self.last do
    if self.data[i]:equals(item, true) then 
      self.data[i] = newItem
      if (self.mode == 2) and self.last - self.first > 0 then
        self:sort(self.data)
      end
      return true 
    end
  end
  return false
end

function Queue:sort(data, p, r)
  quickSort(self.data, self.first, self.last)
end

function Queue:isEmpty()
  return self.last < self.first
end

function Queue:length()
  return self.last - self.first + 1
end


function Queue:contains(item)
  if not self:isEmpty() then
    for i=self.first, self.last do
      if self.data[i]:equals(item) then 
        return true 
      end
    end
  end
  return false
end

function Queue:getData()
  local data = {}
  for i=self.first, self.last do
    table.insert(data, self.data[i])
  end
  return data
end

function Queue:print()
  print('Queue','Mode ' .. self.mode, 'Length ' .. self:length(), '#-Length', #self.data)
  print('Index', 'Node')
  for i=self.first, self.last do
    print(i, self.data[i]:toString())
  end
end