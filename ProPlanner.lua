-- progression planner
dofile("Class.lua")
dofile("node.lua")
dofile("queue.lua")

ProPlanner = {}
local ProPlanner_mt = Class(ProPlanner)

-- 
function ProPlanner:new(domain, problem)  
  local domain = domain
  local problem = problem

  local history = {}
  local start,_ = self:formula_to_strings(problem.init[1])
  local goal, _ = self:formula_to_strings(problem.goal[1])
  
  table.insert(history, start)
  
  return setmetatable({
      domain = domain,
      problem = problem,
      start = start,
      goal = goal,
      history = history,},
    ProPlanner_mt)
end

function ProPlanner:run_bfs()
  local node = Node:new({action=nil, state=self.start, args={nil, nil}, goal=false})
  if table_is_subset(self.goal, node.data.state) then
    return print('Found')
  end
  
  local explored = {}
  local frontier = Queue:new(0)
  frontier:push(node)

  while 1 do
    if frontier:isEmpty() then
      print('Failed')
      return nil
    end
    node = frontier:pop()
    table.insert(explored, node)
    
    for _,v in pairs(self:explore_state(node.data.state)) do
      
      local child = Node:new(v, nil, node)
      if not table_contains(explored, child) and
        not frontier:contains(child) then
        if child.data.goal then 
          print('Found')
          return child
        end
        frontier:push(child)
      end
    end
  end
end

--
function ProPlanner:explore_state(state)
  local successors = {}
  -- 
  for _,act in pairs(self.domain.acts) do
    local args = {}
    for _,ob1 in pairs(self.problem.objs[1]) do
      if table.getn(act.params) == 2 then
        for _,ob2 in pairs(self.problem.objs[1]) do
          if ob1 ~= ob2 then
            table.insert(args, {ob1, ob2})
          end
        end -- for ob2 
      else
        table.insert(args, {ob1})
      end -- if two obs
    end -- for ob1
    
    for _,arg in pairs(args) do
      if self:check_preconditions(state, act, arg[1], arg[2]) == true then
        -- calculate successor for this valid action
        local successor = self:get_successor(state, act, arg[1], arg[2])
        if table_is_subset(self.goal, successor) then
          table.insert(successors, {action=act ,state=successor, args={arg[1], arg[2]}, goal=true})
          return successors
        end
        if self:is_new_state(successor) then
          table.insert(successors, {action=act ,state=successor, args={arg[1], arg[2]}, goal=false})
        end
      end -- if preconditions
    end -- for args
  end -- for actions
  return successors
end


-- 
function ProPlanner:get_successor(state, action, ob, underob)
  local action_add, action_del = self:formula_to_strings(action.effects, ob, underob)
  
  -- calculate successor from action effects
  local suc = table_difference(state, action_del)
  suc = table_union(suc, action_add)
  
  return suc
end

-- 
function ProPlanner:state_to_table(state)
  -- parse to table format
  local t = {}
  for _, v in pairs(state) do
    local pred_name, pred_args = string.match(v, "([%w-_]+)[(]%s*(.*)[)]")
    local args = {}
    for v in string.gmatch(pred_args, "([%a-_]+)") do
      table.insert(args, v)
    end
    table.insert(t, {pred_name, args})
  end
  return t
end

-- 
function ProPlanner:is_new_state(state)
  for _, v in pairs(self.history) do
    if table_equals(state, v) then return false end
  end
  return true
end


-- 
function ProPlanner:check_preconditions(state, action, ob, underob)
  local preconds_add, preconds_del = self:formula_to_strings(action.preconds, ob, underob)
  return table_is_subset(preconds_add, state) and table_is_not_subset(preconds_del, state)
end

-- 
function ProPlanner:formula_to_strings(formula, ob, underob, add, del, pos_or_neg, s)
  local ob = ob
  local underob = underob or nil
  local add = add or {}
  local del = del or {}

  local pos_or_neg = pos_or_neg or "p"
  local s = s or ""
  local rdy = 0

  for i,v in pairs(formula) do
    if type(v) == "string" then
      if ismember(v, lop_table) then
        if string.match(v, "not") then
          pos_or_neg = "n"
        end
      elseif pred_table[v] ~=nil then
        s = v .. "("
      elseif string.match(v, "?ob") then
        if rdy > 0 then
          s = s .. "," .. ob
        else
          s = s .. ob
        end
        rdy = rdy + 1
      elseif string.match(v, "?underob") and underob ~= nil then
        if rdy > 0 then
          s = s .. "," .. underob
        else
          s = s .. underob
        end
        rdy = rdy + 1
      else
        if rdy > 0 then
          s = s .. "," .. v
        else
          s = s .. v
        end
        rdy = rdy + 1
      end
    elseif type(v) == "table" then
      if table.getn(v) == 0 then
        rdy = rdy + 1
      else
        self:formula_to_strings(v, ob, underob, add, del, pos_or_neg, s)
      end
    end
  end
  
  if rdy > 0 then
    s = s .. ")"
    if pos_or_neg == "p" then
      table.insert(add, s)
    elseif pos_or_neg == "n" then
      table.insert(del, s)
    end
  end
  
return add, del
end

-- ### utils ####################################


-- check if set contains element
function table_contains(set, element)
  for _,v in pairs(set) do
    if v == element then 
      return true 
    end
  end
  return false
end

-- check wheather setA is a subset of setB
function table_is_subset(setA, setB)
  for _, v in pairs(setA) do
    if not table_contains(setB, v) then return false end
  end
  return true
end

-- check wheather setA is not a subset of setB
function table_is_not_subset(setA, setB)
  for _, v in pairs(setA) do
    if table_contains(setB, v) then return false end
  end
  return true
end

-- get union of setA and setB
function table_union(setA, setB)
  local unionSet = table_deepcopy(setA)
  for _,v in pairs(setB) do
    if not table_contains(unionSet, v) then
      table.insert(unionSet, v)
    end
  end
  return unionSet
end

function table_difference(setA, setB)
  local diffSet = table_deepcopy(setA)  
  for i = table.getn(diffSet), 1, -1 do
    if table_contains(setB, diffSet[i]) then
      table.remove(diffSet, i)
    end
  end
  return diffSet
end

function table_equals(setA, setB)
  return table_is_subset(setA, setB) and table_is_subset(setB, setA)
end

function table_deepcopy(orig)
    local orig_type = type(orig)
    local copy
    if orig_type == 'table' then
        copy = {}
        for orig_key, orig_value in next, orig, nil do
            copy[table_deepcopy(orig_key)] = table_deepcopy(orig_value)
        end
        setmetatable(copy, table_deepcopy(getmetatable(orig)))
    else -- number, string, boolean, etc
        copy = orig
    end
    return copy
end


